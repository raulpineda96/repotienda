 //Funcion que controla los elementos del tooltip y popover
 $(function(){
 	$("[data-toggle='tooltip']").tooltip();
 	$("[data-toggle='popover']").popover();
 	$('.carousel').carousel({
 		interval: 3000
 	})
 	
 });

 //Funcion que controla los eventos del modal
 $(function(){

 	$("#info").on('show.bs.modal',function(e){
 		console.log('el modal se esta mostrando show');
 		
 		$('#infoBtn').removeClass('btn-outline-success');
 		$('#infoBtn').addClass('btn-primary');
 		 $('#suscripcionbtn').css({'background-color':'rgb(156, 122, 189)'})
 		$('#infoBtn').prop('disabled',true);
 	});

 	$("#info").on('show.bs.modal',function(e){
 		console.log('el modal contacto se esta mostrando shown');
 	});

 	$('#info').on('hide.bs.modal', function(e){
        console.log('el modal suscribirse se esta cerrando hide');
        });

 	$("#info").on('hidden.bs.modal',function(e){
 		console.log('el modal suscribirse se esta cerrando hidden');

 		$("#infoBtn").prop('disabled',true );
 		$("#infoBtn").removeClass('btn-outline-success');
 		$("#infoBtn").addClass('btn-primary');
 		$('#infoBtn').css({'background-color':'#B6E6DE'});
 	});

 });

